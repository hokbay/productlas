import * as React from "react";
import Button from "./Button";
import { FullWidthLeftLine } from "./ElementWrappers";
import Label from "./Label";
import SpaceDivider from "./SpaceDivider";

interface OwnProps {
  gridApi: any;
  columnApi: any;
  totalRowCount: number;
}

interface State {}

export default class GridToolbar extends React.Component<OwnProps, State> {
  render() {
    if (!this.props.columnApi || !this.props.gridApi) {
      return null;
    }
    const rowCount = this.props.totalRowCount + " result(s)";
    return (
      <FullWidthLeftLine>
        <Label size="large" value={rowCount} />
        <SpaceDivider />
        <SpaceDivider />
        <SpaceDivider />

        <Button
          size="x-large"
          text="Export to csv"
          onClick={this.handleCsvExport}
        />
      </FullWidthLeftLine>
    );
  }

  private handleCsvExport = () => {
    this.props.gridApi.exportDataAsCsv({
      fileName: "products",
    });
  };
}
