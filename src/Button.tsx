import styled from "@emotion/styled";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import React from "react";

export const StyledButton = styled.button`
  width: ${(props: InputButtonProps) => {
    switch (props.size) {
      case "x-large":
        return "150px";
      case "large":
        return "100px";
      case "larger":
        return "50";
      default:
    }
  }};

  height: 30px;
  margin-top: 5px;
  border-radius: ${(props: InputButtonProps) => (props.round ? "5px" : "0px")};
  background-color: ${(props: InputButtonProps) => {
    return props.color;
  }};
`;

interface OwnProps {
  text: string;
  type?: string;
  size?: string;
  onClick: () => void;
  round?: boolean;
  color?: string;
}

type InputButtonProps = {
  size?: string;
  round?: boolean;
  color?: string;
};

type Props = OwnProps;

interface State {}

export default class Button extends React.Component<Props> {
  render() {
    return (
      <StyledButton
        size={this.props.size}
        round={this.props.round}
        onClick={this.props.onClick}
        color={this.props.color}
      >
        {this.props.text}
      </StyledButton>
    );
  }
}
