import styled from "@emotion/styled";
import { css } from "@emotion/core";

export const FullWidthWrapper = styled("div")(
  () => css`
    display: flex;
    flex-direction: Column;
    width: 1900px;
    height: 1500px;
    overflow-x: hidden;
  `
);

export const LargeWrapper = styled("div")(
  () => css`
    display: flex;
    flex-direction: Column;
    width: 1200px;
    height: 1200px;
  `
);

export const MediumWrapper = styled("div")(
  () => css`
    display: flex;
    flex-direction: Column;
    width: 800px;
    height: 800px;
  `
);

export const SmallWrapper = styled("div")(
  () => css`
    display: flex;
    flex-direction: Column;
    width: 600px;
    height: 600px;
  `
);

export const FullWidthLeftLine = styled("div")(
  () => css`
    display: flex;
    width: 1800px;
    justify-content: start;
    padding-bottom: 0px;
    padding-left: 10px;
    overflow: hidden;
    background-color: #f5ffff;
  `
);

export const FullWidthRightLine = styled("div")(
  () => css`
    display: flex;
    width: 1800px;
    justify-content: end;
    padding-bottom: 0px;
    overflow: hidden;
    padding-right: 10px;
    background-color: #f5ffff;
  `
);
export const FullWidthCenteredLine = styled("div")(
  () => css`
    display: flex;
    width: 1800px;
    justify-content: center;
    padding-bottom: 0px;
    overflow: hidden;
    background-color: #f5ffff;
  `
);

export const LeftPaddingWrapper = styled("div")(
  () => css`
    margin-left: 10px;
  `
);

export const SplitPaneWrapper = styled.div`
  .Resizer {
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    box-sizing: border-box;
    background: #000;
    opacity: 0.2;
    z-index: 1;
    -moz-background-clip: padding;
    -webkit-background-clip: padding;
    background-clip: padding-box;
  }

  .Resizer:hover {
    -webkit-transition: all 2s ease;
    transition: all 2s ease;
  }

  .Resizer.horizontal {
    height: 11px;
    margin: -5px 0;
    border-top: 5px solid rgba(255, 255, 255, 0);
    border-bottom: 5px solid rgba(255, 255, 255, 0);
    cursor: row-resize;
    width: 100%;
  }

  .Resizer.horizontal:hover {
    border-top: 5px solid rgba(0, 0, 0, 0.5);
    border-bottom: 5px solid rgba(0, 0, 0, 0.5);
  }

  .Resizer.vertical {
    width: 11px;
    margin: 0 -5px;
    border-left: 5px solid rgba(255, 255, 255, 0);
    border-right: 5px solid rgba(255, 255, 255, 0);
    cursor: col-resize;
  }

  .Resizer.vertical:hover {
    border-left: 5px solid rgba(0, 0, 0, 0.5);
    border-right: 5px solid rgba(0, 0, 0, 0.5);
  }
`;
