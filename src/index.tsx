import React from "react";
import ReactDOM from "react-dom";
import Products from "./Products";
import store from "./lib/store";
import { Provider } from "react-redux";

ReactDOM.render(
  <Provider store={store}>
    <Products />
  </Provider>,
  document.querySelector("#root")
);
