export interface SelectOption<T = {}> {
  disabled?: boolean;
  label: string;
  value: any;
  data?: T;
  [key: string]: any;
  id?: number;
}
