import React from "react";
import { AgGridReact } from "ag-grid-react";
import { ValueGetterParams, ICellRendererParams } from "ag-grid-community";
import { SelectOption } from "./Util/SelectOption";
import SelectOptionEditor from "./SelectOptionEditor";

interface OwnProps {
  products: any[];
  categoryOptions: SelectOption[];
  onGridReady: (params: any) => void;
  context: GridContext;
  gridApi: any;
}

export interface GridContext {
  /*  handleProductUpdate: (productId: number, productData: any) => void; */
  handleCategoryChangeWithoutActionDispatch: (
    productId: number,
    productData: any
  ) => void;

  handleProductUpdateWithoutActionDispatch: (
    productId: number,
    productData: any
  ) => void;
}

type Props = OwnProps;

export default class ProductGrid extends React.Component<Props> {
  private createColumn = () => {
    return [
      /*     {
        width: 80,
        resizable: true,
        filter: false,
        sortable: false,
        pinned: "left",
      }, */
      {
        headerName: "ID",
        field: "id",
        resizable: true,
        width: 80,
        sortable: true,
        filter: true,
      },
      {
        headerName: "resource",
        valueGetter: this.getResource,
        resizable: true,
        sortable: true,
        filter: true,
      },
      {
        headerName: "sku",
        field: "sku",
        resizable: true,
        sortable: true,
        filter: true,
      },
      {
        headerName: "name",
        field: "name",
        tooltipField: "name",
        width: 300,
        resizable: true,
        sortable: true,
        filter: true,
        editable: true,
        cellEditor: "agLargeTextCellEditor",
        cellEditorParams: {
          maxLength: "300",
          cols: "50",
          rows: "3",
        },
        onCellValueChanged: (params: ICellRendererParams) => {
          handleNameChange(params, this.props);
        },
      },
      {
        headerName: "cateogry",
        sortable: true,
        filter: true,
        tooltipValueGetter: (params: any) => {
          return this.getCategoryIds(params.data);
        },
        valueGetter: (params: any) => {
          return this.getCategoryIds(params.data);
        },
        editable: true,
        //thie row's data will pass to SelectOptionEditor automatically as props (data-this.props.data)
        cellEditorFramework: SelectOptionEditor,
        cellEditorParams: {
          dataProvider: this.props.categoryOptions,
          change: (productId: number, options: SelectOption[]) => {
            handleCategoryChange(productId, options, this.props);
          },
        },
        width: 300,
      },
      {
        headerName: "description",
        valueGetter: (params: any) => {
          return this.getDescription(params.data);
        },
        resizable: true,
        tooltipValueGetter: (params: any) => {
          return this.getDescription(params.data);
        },
        width: 300,
        filter: true,
        editable: true,
        cellEditor: "agLargeTextCellEditor",
        cellEditorParams: {
          maxLength: "300",
          cols: "50",
          rows: "5",
        },
        onCellValueChanged: (params: ICellRendererParams) => {
          handleDescriptionChange(params, this.props);
        },
      },

      {
        headerName: "regularPrice",
        field: "regular_price",
        valueGetter: (params: any) => {
          return this.getRegularPrice(params.data);
        },
        resizable: true,
        width: 120,
        sortable: true,
        filter: true,
        editable: true,
        onCellValueChanged: (params: ICellRendererParams) => {
          handleRegularPriceChange(params, this.props);
        },
      },
      {
        headerName: "salePrice",
        field: "sale_price",
        valueGetter: (params: any) => {
          return this.getSalePrice(params.data);
        },
        resizable: true,
        width: 120,
        sortable: true,
        filter: true,
        editable: true,
        onCellValueChanged: (params: ICellRendererParams) => {
          handleSalePriceChange(params, this.props);
        },
      },
      {
        headerName: "productUrl",
        field: "external_url",
        resizable: true,
        width: 250,
        sortable: false,
        filter: false,
        editable: true,
        cellEditor: "agLargeTextCellEditor",
        cellEditorParams: {
          maxLength: "300",
          cols: "50",
          rows: "3",
        },
        onCellValueChanged: (params: ICellRendererParams) => {
          handleProductUrlChange(params, this.props);
        },
        cellStyle: (params) =>
          isValidUrl(params.data.external_url.trim())
            ? { "background-color": "white" }
            : { "background-color": "pink" },
      },
      /*  {
        headerName: "productType",
        field: "type",
        resizable: true,
        suppressSorting: true,
        suppressFilter: true,
        sortable: false
      }, */
    ];
  };

  private getResource = (params: ValueGetterParams): string => {
    let str = params.data.sku.replace("_", " ");
    let resource = str.split("_")[0];
    return resource;
  };

  private getCategoryIds = (data: any): any => {
    let categories = "";

    for (let i = 0; i < data.categories.length; i++) {
      categories = categories + data.categories[i].slug;

      if (i < data.categories.length - 1) {
        categories = categories + " | ";
      }
    }

    return categories;
  };

  private getDescription = (data: any): any => {
    let description = data.description
      .replace(/<p>/, "")
      .replace(/<\/p>/g, "")
      .replace(/<br \/>/g, "");

    return description;
  };

  private getRegularPrice = (data: any) => {
    return parseFloat(data.regular_price).toFixed(2);
  };

  private getSalePrice = (data: any) => {
    return data.sale_price === null || data.sale_price === ""
      ? ""
      : parseFloat(data.sale_price).toFixed(2);
  };

  private onGridReady = (e) => {
    this.props.onGridReady(e);
  };

  render() {
    return (
      <AgGridReact
        onGridReady={this.onGridReady}
        columnDefs={this.createColumn()}
        rowData={sortProducts(this.props.products)}
        rowSelection="single"
        rowDeselection
        enableCellTextSelection={true}
        stopEditingWhenGridLosesFocus

        //paginationPageSize={10}
        //pagination={true}
      />
    );
  }
}

const isValidUrl = (url) => {
  if (url.trim().substring(0, 8) !== "https://") {
    return false;
  }
  return true;
};

const handleDescriptionChange = (params: any, props: Props) => {
  props.context.handleProductUpdateWithoutActionDispatch(params.data.id, {
    description: params.data.description.trim(),
  });
};

const handleNameChange = (params: any, props: Props) => {
  props.context.handleProductUpdateWithoutActionDispatch(params.data.id, {
    name: params.data.name.trim(),
  });
};

const handleProductUrlChange = (params: any, props: Props) => {
  if (!isValidUrl(params.data.external_url.trim())) {
    window.alert("Product url must start with https://");
  } else {
    props.context.handleProductUpdateWithoutActionDispatch(params.data.id, {
      external_url: params.data.external_url.trim(),
    });
  }
};

const handleRegularPriceChange = (params: any, props: Props) => {
  props.context.handleProductUpdateWithoutActionDispatch(params.data.id, {
    regular_price: params.data.regular_price,
  });
};

const handleSalePriceChange = (params: any, props: Props) => {
  props.context.handleProductUpdateWithoutActionDispatch(params.data.id, {
    sale_price: params.data.sale_price,
  });
};

const handleCategoryChange = (
  productId: number,
  options: SelectOption[],
  props: Props
) => {
  const categories: any[] = options.map((c) => {
    return { id: c.id, slug: c.label };
  });

  props.context.handleCategoryChangeWithoutActionDispatch(productId, {
    categories: categories,
  });
};

const sortProducts = (products: any[]) => {
  return products.sort((a, b) => {
    return a.id > b.id ? 1 : -1;
  });
};
