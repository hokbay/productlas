import { ColumnApi, GridApi } from "ag-grid-community";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import React from "react";
import { connect, DispatchProp } from "react-redux";
import SplitPane from "react-split-pane";
import { FullWidthWrapper, SplitPaneWrapper } from "./ElementWrappers";
import GridToolbar from "./GridToolBar";
import {
  fetch_categories_thunk_action_creator,
  search_products_thunk_action_creator,
  update_product_without_action_dispatch,
} from "./lib/actions";
import store from "./lib/store";
import ProductGrid from "./ProductGrid";
import SearchPanel from "./SearchPanel";
import { SelectOption } from "./Util/SelectOption";

interface OwnProps {}
interface StateProps {
  // products: any[];
  categories: any[];
}
type Props = OwnProps & StateProps;

interface State {
  columnApi: ColumnApi | any;
  gridApi: GridApi | any;
  products: any[];
}
class Products extends React.Component<Props & DispatchProp, State> {
  state: State = {
    columnApi: null,
    gridApi: null,
    products: [],
  };

  async componentDidMount() {
    //await store.dispatch(fetch_products_thunk_action_creator());
    await store.dispatch(fetch_categories_thunk_action_creator());
    //this.setState({ products: this.props.products });
  }

  render() {
    return (
      <FullWidthWrapper className="ag-theme-balham">
        <SplitPaneWrapper>
          <SplitPane split="vertical" minSize={30} defaultSize="20%">
            <SearchPanel
              searchResource={{ categories: this.getCategorySelectOptions() }}
              handleSearch={this.handleSearch}
            />

            <SplitPane
              split="horizontal"
              minSize={50}
              defaultSize={40}
              allowResize={false}
            >
              <GridToolbar
                columnApi={this.state.columnApi}
                gridApi={this.state.gridApi}
                totalRowCount={
                  (this.state.products && this.state.products.length) || 0
                }
              />
              <ProductGrid
                products={this.state.products}
                categoryOptions={this.getCategorySelectOptions()}
                onGridReady={this.onGridReady}
                gridApi={this.state.gridApi}
                context={{
                  /* handleProductUpdate: this.handleProductUpdate, */
                  handleCategoryChangeWithoutActionDispatch: this
                    .handleCategoryChangeWithoutActionDispatch,
                  handleProductUpdateWithoutActionDispatch: this
                    .handleProductUpdateWithoutActionDispatch,
                }}
              />
            </SplitPane>
          </SplitPane>
        </SplitPaneWrapper>
      </FullWidthWrapper>
    );
  }

  private onGridReady = (e) => {
    this.setState({ gridApi: e.api, columnApi: e.columnApi });
  };

  /*   private handleProductUpdate = async (productId: number, productData: any) => {
    await store.dispatch(
      update_products_thunk_action_creator(productId, productData)
    );
  }; */

  private handleCategoryChangeWithoutActionDispatch = (
    productId: number,
    productData: any
  ) => {
    store.dispatch(
      update_product_without_action_dispatch(productId, productData)
    );

    // we do differently for categories because for category cell editor the select is not part of origin ag grid
    // it is react-select, so the ag grid cannot hold/know the actual value, so we have to udpate the state for the category change
    this.setState({
      products: this.updateCategories(productId, productData.categories),
    });
  };

  private handleProductUpdateWithoutActionDispatch = async (
    productId: number,
    productData: any
  ) => {
    store.dispatch(
      update_product_without_action_dispatch(productId, productData)
    );
  };

  private getCategorySelectOptions = () => {
    const categoryOptions: SelectOption[] = this.props.categories.map((c) => {
      return {
        label: c.slug,
        value: c.slug,
        id: c.id,
      } as SelectOption;
    });
    return categoryOptions;
  };

  private updateCategories = (productId, productData) => {
    const ps: any[] = this.state.products.filter(
      (p: any) => p.id !== productId
    );
    let p: any = this.state.products.filter((p: any) => p.id === productId);
    p = p[0];
    p = { ...p, categories: [...productData] };
    const pp = [...ps, p];

    return pp;
  };

  private handleSearch = async (criteria: any) => {
    let parameter = "";
    if (criteria.sku !== null && criteria.sku !== "") {
      parameter = "&sku=" + criteria.sku;
    } else {
      if (criteria.name !== "") {
        parameter += "&search=" + criteria.name;
      }
      if (criteria.category !== null) {
        parameter += "&category=" + criteria.category.id;
      }
      if (criteria.minPrice !== "") {
        parameter += "&min_price=" + criteria.minPrice;
      }
      if (criteria.maxPrice !== "") {
        parameter += "&max_price=" + criteria.maxPrice;
      }
      if (criteria.beforeDateTime !== "") {
        parameter += "&before=" + criteria.beforeDateTime;
        parameter += "&after=" + criteria.afterDateTime;
      }
    }
    console.log(parameter);
    if (parameter === "") {
      return;
    } else {
      this.props
        .dispatch(await search_products_thunk_action_creator(parameter))
        .then((r) => {
          this.setState({ products: [...r] });
        });
    }
  };
}

const mapStateToProps = (state: any) => {
  return {
    products: state.products,
    categories: state.categories,
  };
};

export default connect(mapStateToProps)(Products);
