import styled from "@emotion/styled";
import { css } from "@emotion/core";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import React from "react";
import DateTimeRangePicker from "@wojtekmaj/react-datetimerange-picker";

const StyledDateTimeRangePicker = styled(DateTimeRangePicker)(
  () => css`
    width: 400px;
    height: 30px;
    margin-bottom: 20px;
    border-radius: 5px;
  `
);
interface OwnProps {
  onChange: (value: Date[]) => void;
}
type Props = OwnProps;

interface State {
  date: Date[];
}

export default class DateTimePicker extends React.Component<Props, State> {
  state = {
    date: [null, null],
  };
  render() {
    return (
      <StyledDateTimeRangePicker
        onChange={this.onChange}
        value={this.state.date}
        isClockOpen={false}
        disableClock={true}
        required={false}
      />
    );
  }
  private onChange = (date) => {
    this.setState({ date }, () => {
      this.props.onChange(this.state.date);
    });
  };
}
