import * as React from "react";
import { ICellEditorParams } from "ag-grid-community";
import styled from "@emotion/styled";
import { css } from "@emotion/core";
import { SelectOption } from "./Util/SelectOption";
import Select from "react-select";

const StyledSelect = styled(Select)(
  () => css`
    width: 300px;
    height: 20;
    minheight: 20;
  `
);

interface State {
  selectedValues: SelectOption[];
}

//this.props.data has the row data passed to here, I do not know why
export interface Props extends ICellEditorParams {
  dataProvider: SelectOption[];
  change?: Function;
  style?: any;
}

export default class SelectOptionEditor extends React.Component<Props, State> {
  constructor(props) {
    super(props);
    this.state = {
      selectedValues: this.getSelectedValues(),
    };
  }

  render() {
    return (
      <StyledSelect
        autoFocus={true}
        options={this.props.dataProvider}
        defaultValue={this.state.selectedValues}
        onChange={this.handleSelectChange}
        closeMenuOnSelect={false}
        isMulti={true}
        size={20}
        onBlur={this.handleOnBlur}
        value={this.state.selectedValues}
      />
    );
  }

  getSelectedValues = () => {
    const selectedCategories = this.props.data.categories.map((c) => {
      return {
        label: c.slug,
        value: c.slug,
        id: c.id,
      } as SelectOption;
    });
    return selectedCategories;
  };

  isPopup = () => {
    return true;
  };

  isCancelAfterEnd = () => {
    return true;
  };

  handleSelectChange = (option: SelectOption[]) => {
    if (option === null || option.length === 0) {
      let resp = window.confirm(
        "All the assigned categories are removed. The product will become uncategoried. Are you sure?"
      );
      if (resp === true) {
        let opt: SelectOption[] = this.props.dataProvider.filter((c) => {
          return c.label === "uncategorized";
        });

        this.setState({
          selectedValues: [...opt],
        });
      }
    } else {
      this.setState({
        selectedValues: [...option], // this options contains all selected options currently
      });
    }

    //this.props.api.stopEditing();
  };

  handleOnBlur = () => {
    this.props.change(this.props.data.id, this.state.selectedValues);
  };
}
