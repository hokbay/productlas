import styled from "@emotion/styled";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import React from "react";

export const StyledInput = styled.input`
  width: ${(props: InputStyleProps) => {
    switch (props.width) {
      case "small":
        return "75px";
      case "large":
        return "330px";
      default:
        return "330px";
    }
  }};
  margin-top: 2px;
  height: 30px;
  border-radius: 4px;
`;

interface OwnProps {
  type: string;
  size?: string;
  placeholder?: string;
  onChange?: (value: string) => void;
  onBlur?: () => void;
}

type InputStyleProps = {
  width?: string;
};

type Props = OwnProps;

interface State {}

export default class TextInput extends React.Component<Props> {
  render() {
    return (
      <StyledInput
        type={this.props.type}
        width={this.props.size}
        placeholder={this.props.placeholder}
        onChange={this.onChange}
        onBlur={this.props.onBlur}
      ></StyledInput>
    );
  }

  private onChange = (e) => {
    this.props.onChange(e.target.value);
  };
}
