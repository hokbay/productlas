import styled from "@emotion/styled";
import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import React from "react";

export const StyledSpan = styled.span`
  font-size: ${(props: SpanStyleeProps) => {
    switch (props.size) {
      case "large":
        return "large";
      case "medium":
        return "medium";
      case "small":
        return "small";
      default:
    }
  }};
  color: ${(props: SpanStyleeProps) => {
    return props.color;
  }};

  margin-top: 5px;
`;

interface OwnProps {
  value: string | number;
  size?: string;
  color?: string;
}

type SpanStyleeProps = {
  size: string;
  color?: string;
};

type Props = OwnProps;

interface State {}

export default class Label extends React.Component<Props> {
  render() {
    return (
      <StyledSpan size={this.props.size} color={this.props.color}>
        {this.props.value}
      </StyledSpan>
    );
  }
}
