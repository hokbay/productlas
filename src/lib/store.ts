import { createStore, applyMiddleware } from "redux";
import asyncReducer from "./reducers";
import thunk from "redux-thunk";

// can put reducer or combined reducer here
const store = createStore(asyncReducer, applyMiddleware(thunk));

export default store;
