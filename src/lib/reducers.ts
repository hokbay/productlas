const initialState: any = {
  products: [],
  categories: [],
  isFetching: false,
  isError: false,
};

const asyncReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case "FETCHING_SOMETHING":
      return Object.assign({}, state, {
        isFetching: true,
        products: [],
        isError: false,
      });
    case "FETCH_PRODUCTS":
      return Object.assign({}, state, {
        products: action.products,
        isFetching: false,
        isError: false,
      });
    case "UPDATE_PRODUCT":
      return Object.assign({}, state, {
        products: updateProducts(state, action.productData),
        isFetching: false,
        isError: false,
      });
    case "FETCH_CATEGORIES":
      return Object.assign({}, state, {
        categories: action.categories,
        isFetching: false,
        isError: false,
      });
    case "RECEIVE_ERROR":
      return Object.assign({}, state, {
        isError: true,
        isFetching: false,
      });
    default:
      return state;
  }
};

const updateProducts = (state: any, product: any) => {
  const ps: any[] = state.products.filter((p: any) => p.id !== product.id);
  return [...ps, product];
};

export default asyncReducer;
