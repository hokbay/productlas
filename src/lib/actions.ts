import store from "./store";
import axios from "axios";

export const fetching_something = () => {
  return {
    type: "FETCHING_SOMETHING",
  };
};

export const fetch_products = (products: any[]) => {
  return {
    type: "FETCH_PRODUCTS",
    products: products,
  };
};

export const update_product = (productData: any) => {
  return {
    type: "UPDATE_PRODUCT",
    productData: productData,
  };
};

export const fetch_categories = (categories: any[]) => {
  return {
    type: "FETCH_CATEGORIES",
    categories: categories,
  };
};

export const receive_error = () => {
  return {
    type: "RECEIVE_ERROR",
  };
};

export const fetch_products_thunk_action_creator = () => {
  const token = Buffer.from(
    `ck_10effb70d3934a2352bd72a830307b9735cef206:cs_65ce7a525bdcb2e71083a41e5da3614ec4184d4c`,
    "utf8"
  ).toString("base64");
  store.dispatch(fetching_something());

  return function (dispatch: any, getState: any) {
    return axios
      .get(`https://hokhub.com/wp-json/wc/v3/products?per_page=100`, {
        headers: {
          Authorization: `Basic ${token}`,
        },
      })
      .then((result) => {
        console.log(result.data);
        if (result.status !== 200) {
          throw new Error("Fetch products failed!!");
        } else dispatch(fetch_products(result.data));
      })
      .catch((err) => dispatch(receive_error()));
  };
};

export const update_products_thunk_action_creator = (
  productId: number,
  productData: any
) => {
  const token = Buffer.from(
    `ck_10effb70d3934a2352bd72a830307b9735cef206:cs_65ce7a525bdcb2e71083a41e5da3614ec4184d4c`,
    "utf8"
  ).toString("base64");

  return function (dispatch: any, getState: any) {
    return axios
      .put(
        `https://hokhub.com/wp-json/wc/v3/products/${productId}`,
        productData,
        {
          headers: {
            Authorization: `Basic ${token}`,
          },
        }
      )
      .then((result) => {
        if (result.status !== 200 && result.status !== 201) {
          throw new Error("update product failed");
        } else dispatch(update_product(result.data));
      })
      .catch((err) => dispatch(receive_error()));
  };
};

export const fetch_categories_thunk_action_creator = () => {
  const token = Buffer.from(
    `ck_10effb70d3934a2352bd72a830307b9735cef206:cs_65ce7a525bdcb2e71083a41e5da3614ec4184d4c`,
    "utf8"
  ).toString("base64");

  return function (dispatch: any, getState: any) {
    return axios
      .get(
        `https://hokhub.com/wp-json/wc/v3/products/categories?per_page=100`,
        {
          headers: {
            Authorization: `Basic ${token}`,
          },
        }
      )
      .then((result) => {
        //console.log(result.data);
        if (result.status !== 200) {
          throw new Error("Fetch categories failed!!");
        } else dispatch(fetch_categories(result.data));
      })
      .catch((err) => dispatch(receive_error()));
  };
};

export const update_product_without_action_dispatch = (
  productId: number,
  productData: any
) => {
  const token = Buffer.from(
    `ck_10effb70d3934a2352bd72a830307b9735cef206:cs_65ce7a525bdcb2e71083a41e5da3614ec4184d4c`,
    "utf8"
  ).toString("base64");

  return function (dispatch: any, getState: any) {
    return axios
      .put(
        `https://hokhub.com/wp-json/wc/v3/products/${productId}`,
        productData,
        {
          headers: {
            Authorization: `Basic ${token}`,
          },
        }
      )
      .then((result) => {
        if (result.status !== 200 && result.status !== 201) {
          throw new Error("update product failed");
        }
      })
      .catch((err) => dispatch(receive_error()));
  };
};

export const search_products_thunk_action_creator = async (
  parameter: string
): Promise<any> => {
  const token = Buffer.from(
    `ck_10effb70d3934a2352bd72a830307b9735cef206:cs_65ce7a525bdcb2e71083a41e5da3614ec4184d4c`,
    "utf8"
  ).toString("base64");
  store.dispatch(fetching_something());
  return function (dispatch: any, getState: any) {
    return axios
      .get(
        `https://hokhub.com/wp-json/wc/v3/products?per_page=100` + parameter,
        {
          headers: {
            Authorization: `Basic ${token}`,
          },
        }
      )
      .then((result) => {
        return result.data;
      });
  };
};
