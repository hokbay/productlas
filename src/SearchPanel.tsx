import "ag-grid-community/dist/styles/ag-grid.css";
import "ag-grid-community/dist/styles/ag-theme-balham.css";
import React from "react";
import Label from "./Label";
import TextInput from "./TextInput";
import Button from "./Button";
import { LeftPaddingWrapper } from "./ElementWrappers";
import styled from "@emotion/styled";
import { css } from "@emotion/core";
import Select from "react-select";
import { SelectOption } from "./Util/SelectOption";
import DateTimePicker from "./DateTimePicker";
import moment from "moment";

const StyledSelect = styled(Select)(
  () => css`
    width: 335px;
    height: 10;
    minheight: 10;
  `
);

interface OwnProps {
  searchResource: {
    categories: SelectOption[];
  };
  handleSearch: (criteria: any) => void;
}

interface StateProps {}

type Props = OwnProps & StateProps;

interface State {
  criteria: any;
  errorMessage: string;
}

export default class SearchPanel extends React.Component<Props, State> {
  state = {
    criteria: {
      name: "",
      category: null,
      sku: "",
      minPrice: "",
      maxPrice: "",
      beforeDateTime: "",
      afterDateTime: "",
    },
    errorMessage: "",
  };
  render() {
    return (
      <LeftPaddingWrapper>
        <div>
          <br />
          <Label size="large" value="Name" />
          <br />
          <TextInput
            type="text"
            size="large"
            placeholder="   type name..."
            onChange={this.handleNameChange}
          ></TextInput>
          <br />
          <br />
          <Label size="large" value="Category" />
          <br />
          <StyledSelect
            autoFocus={true}
            options={this.props.searchResource.categories}
            onChange={this.handleCategoryChange}
            closeMenuOnSelect={true}
            isMulti={false}
            isClearable={true}
            size={10}
          />
          <br />
          <Label size="large" value="Sku" />
          <br />
          <TextInput
            type="text"
            size="large"
            placeholder="   type sku..."
            onChange={this.handleSkuChange}
          ></TextInput>
          <br />
          <br />
          <Label size="large" value="Price Range" />
          <br />
          <TextInput
            type="text"
            size="small"
            onChange={this.handleMinPriceChange}
            onBlur={this.handlePriceBlur}
          ></TextInput>
          <Label size="small" value="  TO  " />
          <TextInput
            type="text"
            size="small"
            onChange={this.handleMaxPriceChange}
            onBlur={this.handlePriceBlur}
          ></TextInput>
          <br />
          <br />
          <Label size="large" value="Creation Date" />
          <DateTimePicker onChange={this.handleDateTimeRangeChange} />
          <br />
          <Label size="small" value={this.state.errorMessage} color="red" />
          <br />
          <Button
            text="Search"
            size="large"
            onClick={this.handleSearch}
            round={true}
            color="#f5ffff"
          />
        </div>
      </LeftPaddingWrapper>
    );
  }
  private handleNameChange = (value: string) => {
    this.setState({
      criteria: {
        ...this.state.criteria,
        name: value.trim(),
      },
    });
  };

  private handleSkuChange = (value: string) => {
    this.setState({
      criteria: {
        ...this.state.criteria,
        sku: value.trim(),
      },
    });
  };

  private handleMinPriceChange = (value: string) => {
    this.setState({
      criteria: {
        ...this.state.criteria,
        minPrice: value.trim(),
      },
    });
  };

  private handleMaxPriceChange = (value: string) => {
    this.setState({
      criteria: {
        ...this.state.criteria,
        maxPrice: value.trim(),
      },
    });
  };

  private handleCategoryChange = (option: SelectOption) => {
    let category = null;

    if (option) {
      category = { id: option.id, slug: option.label };
    }

    this.setState({
      criteria: {
        ...this.state.criteria,
        category: category,
      },
    });
  };

  private handlePriceBlur = () => {
    if (
      (this.state.criteria.minPrice !== "" &&
        isNaN(parseFloat(this.state.criteria.minPrice))) ||
      (this.state.criteria.maxPrice !== "" &&
        isNaN(parseFloat(this.state.criteria.maxPrice))) ||
      (this.state.criteria.minPrice !== "" &&
        this.state.criteria.maxPrice !== "" &&
        parseFloat(this.state.criteria.minPrice) >
          parseFloat(this.state.criteria.maxPrice))
    ) {
      this.setState({
        errorMessage:
          "Either price is not a valid number or min price is greater than max price.",
      });
    } else {
      this.setState({
        errorMessage: "",
      });
    }
  };

  private handleDateTimeRangeChange = (date: Date[]) => {
    if (date) {
      const beforeStr: string = moment(date[1]).format(
        "YYYY-MM-DDTHH:MM:ss.SSS[Z]"
      );
      const afterStr: string = moment(date[0]).format(
        "YYYY-MM-DDTHH:MM:ss.SSS[Z]"
      );
      this.setState({
        criteria: {
          ...this.state.criteria,
          beforeDateTime: beforeStr,
          afterDateTime: afterStr,
        },
      });
    } else {
      this.setState({
        criteria: {
          ...this.state.criteria,
          beforeDateTime: "",
          afterDateTime: "",
        },
      });
    }
  };

  private handleSearch = () => {
    this.props.handleSearch(this.state.criteria);
  };
}
